//
//  BankTests.m
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Bank.h"

@interface BankTests : XCTestCase

@end

@implementation BankTests

- (void)testBankFlow {

    Bank *bank = [Bank new];
    
    XCTAssertEqual(bank.allAccounts.count, 0);
    XCTAssertFalse([bank hasAccountWithID:1]);
    
    if ([bank createAccountWithID:1 balance:0]) {
        
        // now the account should exist
        XCTAssertEqual(bank.allAccounts.count, 1);
        XCTAssertTrue([bank hasAccountWithID:1]);

        Account *account = bank.allAccounts[0];
        XCTAssertEqual(account.accountID, 1);
        
        // try to create again... it should fail
        XCTAssertFalse([bank createAccountWithID:1 balance:0]);
        
        // this should work
        XCTAssertTrue([bank runTransactionWithAccountID:1 value:1]);
        
        // this shouldn't work: trasaction overflow
        XCTAssertFalse([bank runTransactionWithAccountID:1 value:NSIntegerMax]);

        // this shouldn't: the account doesn't exist
        XCTAssertFalse([bank runTransactionWithAccountID:2 value:0]);
        
    } else {
        XCTFail(@"can't create any bank account!");
    }
}

@end
