//
//  AccountTests.m
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Account.h"

@interface AccountTests : XCTestCase

@end

@implementation AccountTests

- (void)testInit {
    
    XCTAssertThrows([Account new]); // can't instantiate with the default init!
    
    Account *account1 = [[Account alloc] initWithID:0 balance:0];
    XCTAssertNotNil(account1);
    XCTAssertEqual(account1.accountID, 0);
    XCTAssertEqual(account1.balance, 0);
        
    Account *account2 = [[Account alloc] initWithID:666 balance:777];
    XCTAssertNotNil(account2);
    XCTAssertEqual(account2.accountID, 666);
    XCTAssertEqual(account2.balance, 777);
}

- (void)testTransactions {
    
    Account *account = [[Account alloc] initWithID:0 balance:0];
    
    // normal 'zero' transaction
    XCTAssertTrue([account runTransactionWithValue:0]);
    XCTAssertEqual(account.balance, 0);
    
    // normal positive transaction
    XCTAssertTrue([account runTransactionWithValue:NSIntegerMax]);
    XCTAssertEqual(account.balance, NSIntegerMax);
    
    // failed positive transaction - overflow
    XCTAssertFalse([account runTransactionWithValue:1]);
    // the balance should be kept
    XCTAssertEqual(account.balance, NSIntegerMax);
    
    // negative transaction with positive final balance
    XCTAssertTrue([account runTransactionWithValue:-NSIntegerMax]);
    XCTAssertEqual(account.balance, 0);
    
    // negative transaction with negative final balance - it should sum the fine
    XCTAssertTrue([account runTransactionWithValue:-1]);
    XCTAssertEqual(account.balance, -501);
    
    // zero/positive transactions with negative balance - it shouldn't sum the fine
    XCTAssertTrue([account runTransactionWithValue:0]);
    XCTAssertEqual(account.balance, -501);
    XCTAssertTrue([account runTransactionWithValue:1]);
    XCTAssertEqual(account.balance, -500);
    
    // failed negative transaction - underflow
    XCTAssertFalse([account runTransactionWithValue:NSIntegerMin]);
    // the balance should be kept
    XCTAssertEqual(account.balance, -500);
}

- (void)testCopy {

    Account *account = [[Account alloc] initWithID:1 balance:2];
    Account *anotherReference = account;
    XCTAssert(account == anotherReference);
    
    Account *accountCopy = account.copy;
    XCTAssert(account != accountCopy);
    XCTAssertEqual(account.accountID, accountCopy.accountID);
    XCTAssertEqual(account.balance, accountCopy.balance);
}

@end
