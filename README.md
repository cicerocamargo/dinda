# README #

### About the solution ###

This is an XCode project for a command line tool that resolves the Dinda developer challenge.
It loads account data from a CSV file into a Bank object, then runs all the transactions from another CSV file also via the Bank object, which in turn encapsulates and manipulates Account objects.

### How do I get set up? ###

The project was built with XCode 7.2 and the minimum requirement for the executable is MacOS 10.6
There's one target to run the executable (the 'run' action already passes the CSV files included in the project as parameters) and one target for running the unit tests.

### Who do I talk to? ###

If you find any problems in opening/executing the project, pleas send an e-mail to camargo.cicero@gmail.com.