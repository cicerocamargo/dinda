//
//  Account.h
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject <NSCopying>

@property (nonatomic, readonly) NSUInteger accountID;   ///< the ID of the account
@property (nonatomic, readonly) NSInteger  balance;     ///< the balance of the account
@property (nonatomic, readonly) NSInteger  fine;        ///< value added to balance after a transaction with negative outcome

- (instancetype)initWithID:(NSUInteger)accountID balance:(NSInteger)balance;

/**
 * This method tries to add 'value' to 'balance'
 * @returns a success boolean value
 * @discussion a fine can be summed to the transaction; the transaction can fail upon underflow/overflow detections
 */
- (BOOL)runTransactionWithValue:(NSInteger)value;

@end
