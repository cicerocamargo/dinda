//
//  Account.m
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import "Account.h"

@interface Account ()

// mutable versions of the properties
@property (nonatomic, assign) NSUInteger accountID;
@property (nonatomic, assign) NSInteger  balance;

@end

@implementation Account

- (id)copyWithZone:(NSZone *)zone {
    
    Account *copy = [[self class] allocWithZone:zone];
    copy->_accountID = self.accountID;
    copy->_balance   = self.balance;
    return copy;
}

- (instancetype)init {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for this class"
                                 userInfo:nil];
    return nil;
}

- (instancetype)initWithID:(NSUInteger)accountID balance:(NSInteger)balance {
    
    self = [super init];
    if (self) {
        _accountID = accountID;
        _balance   = balance;
    }
    return self;
}

- (BOOL)runTransactionWithValue:(NSInteger)value {

    @try {
        NSInteger newBalance = [self safelyAdd:self.balance to:value];
        BOOL isDebit = value < 0;
        if (isDebit && newBalance < 0) { // sum the fine
            newBalance = [self safelyAdd:newBalance to:self.fine];
        }
        self.balance = newBalance;
        return YES;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return NO;
    }
}

#pragma mark - private

- (NSInteger)safelyAdd:(NSInteger)a to:(NSInteger)b {

        if ((a > 0 && b > NSIntegerMax - a) || (a < 0 && b < NSIntegerMin - a)) {
            [NSException raise:@"Overflow/Underflow attempt"
                        format:@"the value of %lld + %lld is out of what an NSInteger can hold", (long long)a, (long long)b];
        }
        return a + b;
}

- (NSInteger)fine {
    
    return -500; // R$ -5,00
}

@end