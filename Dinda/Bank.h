//
//  Bank.h
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Account.h"

@interface Bank : NSObject

/**
 * @returns YES if an account with 'accountID' exists in this bank
 */
- (BOOL)hasAccountWithID:(NSUInteger)accountID;

/**
 * @returns YES if an account with 'accountID' was created with the initial 'balance';
 *      if an account with the same ID already exists, it returns NO
 */
- (BOOL)createAccountWithID:(NSUInteger)accountID balance:(NSInteger)balance;

/**
 * @returns YES if a transaction with 'value' was executed correctly in the account with 'accountID';
 *      if the account is not found, returns NO
 */
- (BOOL)runTransactionWithAccountID:(NSUInteger)accountID value:(NSInteger)value;

/**
 * @returns an array with the copies of all account objects held internally
 */
- (NSArray *)allAccounts;

@end
