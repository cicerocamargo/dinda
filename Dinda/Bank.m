//
//  Bank.m
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import "Bank.h"

@interface Bank ()

@property (nonatomic, strong) NSMutableDictionary *accountsDictionary; ///< holds all the accounts, indexed by the 'accountID'

@end

@implementation Bank

- (instancetype)init {
    
    self = [super init];
    if (self) {
        _accountsDictionary = [NSMutableDictionary new];
    }
    return self;
}

- (BOOL)hasAccountWithID:(NSUInteger)accountID {
    
    return [self accountWithID:accountID] != nil;
}

- (BOOL)createAccountWithID:(NSUInteger)accountID balance:(NSInteger)balance {
    
    if (![self hasAccountWithID:accountID]) {
        [self setAccount:[[Account alloc] initWithID:accountID balance:balance] withID:accountID];
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)runTransactionWithAccountID:(NSUInteger)accountID value:(NSInteger)value {

    if ([self hasAccountWithID:accountID]) {
        return [[self accountWithID:accountID] runTransactionWithValue:value];
    }
    return NO;
}

- (NSArray *)allAccounts {
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"accountID" ascending:YES];
    NSArray *accountCopies = [[NSArray alloc] initWithArray:self.accountsDictionary.allValues copyItems:YES];
    return [accountCopies sortedArrayUsingDescriptors:@[sortDescriptor]];
}

#pragma mark - private (dictionary handling helpers)

- (Account *)accountWithID:(NSUInteger)accountID {
    
    return self.accountsDictionary[@(accountID)];
}

- (void)setAccount:(Account *)account withID:(NSUInteger)accountID {

    self.accountsDictionary[@(accountID)] = account;
}

@end
