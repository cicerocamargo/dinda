//
//  main.m
//  Dinda
//
//  Created by Cícero Camargo on 05/04/16.
//  Copyright © 2016 Cícero Camargo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bank.h"

/**
 * Try to open a file for reading with the file name passed as parameter.
 * @return a file pointer
 * @warning it crashes the app if the file is not opened correctly
 */
FILE *readFile(const char* fileName);

/**
 * Parses the accounts file and loads the values into the 'bank'
 * @warning this function runs some asserts that can crash the app
 */
void loadAccounts(const char* fileName, Bank *bank);

/**
 * Parses the transactions file and runs them into the 'bank'
 * @warning this function runs some asserts that can crash the app
 */
void runTransactions(const char* fileName, Bank *bank);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        if (argc < 3) {
            fprintf(stderr, "Input files missing!\n");
            exit(EXIT_FAILURE);
        }
        
        Bank *bank = [Bank new];
        loadAccounts(argv[1], bank);
        runTransactions(argv[2], bank);
        
        // print the results
        for (Account *account in bank.allAccounts) {
            printf("%lu,%ld\n", account.accountID, account.balance);
        }
        
        exit(EXIT_SUCCESS);
    }
}

FILE *readFile(const char* fileName) {
    
    FILE *fp = fopen(fileName, "r");
    if (fp == NULL) {
        fprintf(stderr, "Error reading file '%s'!\n", fileName);
        exit(EXIT_FAILURE);
        return NULL;
    }
    return fp;
}

void loadAccounts(const char* fileName, Bank *bank) {
    
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    
    fp = readFile(fileName);
    while ((read = getline(&line, &len, fp)) != -1) {
        NSUInteger accountID;
        NSInteger balance;
        sscanf(line, "%lu,%ld", &accountID, &balance);
        BOOL success = [bank createAccountWithID:accountID balance:balance];
        assert(success);
    }
    
    fclose(fp);
    if (line) {
        free(line);
    }
}

void runTransactions(const char* fileName, Bank *bank) {
    
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    
    fp = readFile(fileName);
    while ((read = getline(&line, &len, fp)) != -1) {
        NSUInteger accountID;
        NSInteger value;
        sscanf(line, "%lu,%ld", &accountID, &value);
        BOOL success = [bank runTransactionWithAccountID:accountID value:value];
        assert(success);
    }
    
    fclose(fp);
    if (line) {
        free(line);
    }
}
